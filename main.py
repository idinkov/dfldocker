import os
import shutil
import time
import subprocess
import sys
from subprocess import Popen
import signal
import psutil
from notifiers import get_notifier
basePath = os.path.dirname(os.path.abspath(__file__))

purpose = os.environ["PURPOSE"]
pushoverUser = os.environ["PUSHOVER_USER"]
pushoverToken = os.environ["PUSHOVER_TOKEN"]
inputType = os.environ["INPUT_TYPE"]  # @param ["workspace", "data_src", "data_dst", "data_src aligned", "data_dst aligned", "models"]
inputFetchType = os.environ["INPUT_FETCH_TYPE"]  # @param ["direct_http", "yt-dlp"]
inputPassword = os.environ["INPUT_PASSWORD"]
inputUrl = os.environ["INPUT_URL"]
inputConnections = os.environ["INPUT_CONNECTIONS"]

inputArchiveName = inputType + ".zip"  # @param {type:"string"}
inputArchiveLocation = basePath + "/" + inputArchiveName  # @param {type:"string"}

def notify(message):
    print(message)
    p = get_notifier('pushover')
    p.notify(user=pushoverUser, token=pushoverToken, message=message)

def executeBackgroundProcess(processLine):
    notify(">>>> " + processLine + " [STARTING BACKGROUND PROCESS]")
    startTime = time.time()
    p = Popen(processLine.split())
    endTime = time.time()
    executionTime = endTime - startTime
    executionTime = '%.3f' % (executionTime)
    notify(">>>> " + processLine + " (" + str(executionTime) + "s) PID:" + str(p.pid))
    return p

def executeProcess(processLine):
    notify(">>>> " + processLine + ": [STARTING]")
    startTime = time.time()
    with subprocess.Popen(processLine, stdout=subprocess.PIPE, stderr=None, shell=True) as process:
        output = process.communicate()[0].decode("utf-8")

    endTime = time.time()
    executionTime = endTime - startTime
    executionTime = '%.3f' % (executionTime)
    notify(">>>> " + processLine + ": [COMPLETED] (" + str(executionTime) + "s) ")
    return output

def unzipAndRemove(archiveLocation, archivePassword, outputLocation):
    # Unzip archive
    executeProcess("7z x " + archiveLocation + " -o" + outputLocation + " -aoa -p" + archivePassword)

def zipSubdirectory(subdirectory, inputLocation, archiveOutput, archivePassword):
    executeProcess("(cd " + subdirectory + " && zip -0 -r --password " + archivePassword + " " + archiveOutput + " " + inputLocation+")")

def zip(inputLocation, archiveOutput, archivePassword):
    executeProcess("zip -0 -r --password " + archivePassword + " " + archiveOutput + " " + inputLocation)

def upload(file, host):
    return executeProcess("curl --upload-file " + file + " http://" + host + "/workspace.zip")

def downloadVideo():
    pass

startTime = time.time()
notify("Started job!")

# 1) Import Input
if inputFetchType == "direct_http":
    executeProcess("axel -n " + inputConnections + " -o " + inputArchiveName + " " + inputUrl)
elif inputFetchType == "yt-dlp":
    executeProcess("yt-dlp " + inputUrl + " -o " + "workspace/data_src.mp4")

# 1.1) Unpack archive
if inputType == "workspace":
    unzipAndRemove(inputArchiveLocation, inputPassword, ".")
elif inputType == "data_src":
    unzipAndRemove(inputArchiveLocation, inputPassword, "workspace")
elif inputType == "data_dst":
    unzipAndRemove(inputArchiveLocation, inputPassword, "workspace/data_dst")
elif inputType == "data_src_aligned":
    unzipAndRemove(inputArchiveLocation, inputPassword, "workspace/data_src")
elif inputType == "data_dst_aligned":
    unzipAndRemove(inputArchiveLocation, inputPassword, "workspace/data_dst")
elif inputType == "models":
    unzipAndRemove(inputArchiveLocation, inputPassword, "workspace")
elif inputType == "download_video_dst":
    downloadVideo()
elif inputType == "download_video_src":
    downloadVideo()

# 2) Process. This one is special and we will make a wait hook after the initial start
if purpose == "train":
    duration = int(os.environ["TRAIN_DURATION"]) # Duration is in minutes, tells how much time is needed
    pid = executeBackgroundProcess(sys.executable + " " + basePath + "/DeepFaceLab/main.py train --training-data-src-dir " + basePath + "/workspace/data_src/aligned --training-data-dst-dir " + basePath + "/workspace/data_dst/aligned --pretraining-data-dir " + basePath + "/DeepFaceLab/pretrain --model-dir " + basePath + "/workspace/model --model SAEHD --silent-start")

    processCompleted = False
    allowedTimeSeconds = duration*60
    startTime = time.time()
    while processCompleted is False:
        time.sleep(10)
        if (time.time() - startTime) > allowedTimeSeconds:
            pid.send_signal(signal.SIGINT)
            time.sleep(60)
            processCompleted = True

        if psutil.pid_exists(pid.pid) == False:
            processCompleted = True

if purpose == "extract_frames":
    executeProcess("python DeepFaceLab/main.py videoed extract-video --input-file workspace/data_dst.* --output-dir workspace/data_dst/ --fps 0 --output-ext jpg")

# 3) Compress & Upload Output
outputFile = "output.zip"
outputType = os.environ["OUTPUT_TYPE"] # @param ["data_dst", "data_dst_aligned", "model"]

additionalInputFolder = ""
if outputType == "data_dst":
    additionalInputFolder = "data_dst/"
elif outputType == "data_dst_aligned":
    additionalInputFolder = "data_dst/aligned/"
elif outputType == "model":
    additionalInputFolder = "model/"


def finalZipAndNotify(subdirectory, outputInputFolder, outputArchivePath):
    zipSubdirectory(subdirectory, outputInputFolder, outputArchivePath, inputPassword)
    link = upload(subdirectory + "/" + outputArchivePath, os.environ["UPLOAD_HOST"])
    executionTime = '%.3f' % (time.time() - startTime)
    notify("[UPLOADED] (" + executionTime + ") Download output at: " + link)

# 3) Execute
outputSplitMode = int(os.environ["OUTPUT_SPLIT_MODE"])
finalWorkspaceFolder = "workspace/" + additionalInputFolder

# Start fetching batch data
totalFiles = []
for subdir, dirs, files in os.walk(finalWorkspaceFolder):
    for file in files:
        ext = os.path.splitext(file)[-1].lower()
        fileName = os.path.basename(file)
        if ext in ('.jpg', '.png', '.dat', '.npy', '.txt'):
            totalFiles.append(os.path.join(subdir, file))

numberFiles = len(totalFiles)
batchSize = round(numberFiles/outputSplitMode)

def ig_f(dir, files):
    return [f for f in files if os.path.isfile(os.path.join(dir, f))]

# Start batch work
for batch in range(outputSplitMode):
    start = batchSize * batch
    if batch == outputSplitMode:
        end = numberFiles
    else:
        end = start + batchSize

    # Create new workspace folder
    batchFolder = "workspace_" + str(batch)
    newWorkspaceFolder = batchFolder + "/workspace"
    if os.path.exists(batchFolder):
        shutil.rmtree(batchFolder, ignore_errors=True)
    os.mkdir(batchFolder)
    shutil.copytree(basePath + "/workspace", basePath + "/" + newWorkspaceFolder, ignore=ig_f)

    # Move the chosen one files to selected new folder
    for currentFile in range((end-start)):
        currentAbsoluteFile = currentFile + start
        if currentAbsoluteFile < numberFiles:
            fileLocation = basePath + "/" + totalFiles[currentAbsoluteFile]
            newLocation = basePath + "/" + batchFolder + "/" + totalFiles[currentAbsoluteFile]
            shutil.move(fileLocation, newLocation)

    finalZipAndNotify(batchFolder, "workspace", "output_" + str(batch+1) + ".zip")

