#!/bin/bash
while getopts a:b:c:d:e:f:g:h:i:j:k:l: flag
do
    case "${flag}" in
        a) purpose=${OPTARG};;
        b) trainDuration=${OPTARG};;
        c) inputType=${OPTARG};;
        d) outputType=${OPTARG};;
        e) outputSplitMode=${OPTARG};;
        f) inputFetchType=${OPTARG};;
        g) inputUrl=${OPTARG};;
        h) inputPassword=${OPTARG};;
        i) inputConnections=${OPTARG};;
        j) uploadHost=${OPTARG};;
        k) pushoverUser=${OPTARG};;
        l) pushoverToken=${OPTARG};;
    esac
done
echo "Purpose: $purpose";
echo "Train Duration: $trainDuration";
echo "Input Type: $inputType";
echo "Output Type: $outputType";
echo "Output Split Mode: $outputSplitMode";
echo "Input Fetch Type: $inputFetchType";
echo "Input Url: $inputUrl";
echo "Input Password: $inputPassword";
echo "Input Connections: $inputConnections";
echo "Upload Host: $uploadHost";
echo "Pushover User: $pushoverUser";
echo "Pushover Token: $pushoverToken";

export PURPOSE=$purpose
export TRAIN_DURATION=$trainDuration
export INPUT_TYPE=$inputType
export OUTPUT_TYPE=$outputType
export OUTPUT_SPLIT_MODE=$outputSplitMode
export INPUT_FETCH_TYPE=$inputFetchType # [direct_http, youtube-dl]
export INPUT_URL=$inputUrl
export INPUT_PASSWORD=$inputPassword
export INPUT_CONNECTIONS=$inputConnections
export UPLOAD_HOST=$uploadHost
export PUSHOVER_USER=$pushoverUser
export PUSHOVER_TOKEN=$pushoverToken

export LC_ALL='en_US.utf8'
python main.py